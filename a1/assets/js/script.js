let courseArr = [{
	     "id" : "001"
		,"name" : "Algebra"
		,"description" : "Math course"
		,"price" : 1000
		,"isActive" : true
},
{
	     "id" : "002"
		,"name" : "Science"
		,"description" : "Science course"
		,"price" : 2500
		,"isActive" : true
}];

const postCourse = (courseArr,id,name,description, price,isActive) => {

	let course = new Object();

	course.id = id;
	course.name = name;
	course.description = description;
	course.price = price;
	course.isActive = isActive;

	// let course = `{id:${this.id},name : ${this.name},description : ${this.description},price:${this.price}, isActive: ${this.isActive}}`;

	console.log(typeof(course)); //Object

	courseArr.push(course);
	
	console.log(courseArr[2]); //shows added object

	console.log(`You have created ${course.name }. The price is ${course.price}`);

}

postCourse(courseArr,'003','Philippine History','Philippine History course', 3000, true);

const finCourse = (num) => {

	 this.num = num;

	let found = courseArr.find((a) => a.id === this.num )
	console.log(found);
}

finCourse("001");

const delCourse = () => {
	courseArr.pop();
	console.log(courseArr);
}

delCourse();