/*
  convert the following function into an arrow function and display the result in console.

let numbers = [1, 2, 3, 4, 5];

// let allValid = numbers.every(function(number){
// 	return (number < 3)
// });	

const allValid = numbers.every(number => console.log(number<3))

// let filterValid = numbers.filter(function(number){
// 	return (number < 3)
// })

const filterValid = numbers.filter(number => console.log(number<3));

// let numberMap = numbers.map(function(number){
// 	return number * number
// })

const numberMap = numbers.map (number =>console.log(number*number));
*/


// ------------------------------------------

// let assets = [{
// 		 "id" : "001"
// 		,"name" : "Zhongli"
// 		,"description" : "God of Contract"
// 		,"isAvailable" : false
// 		,"dateAdded" : "10-13-2021"
// 	},


// 	{
// 		 "id" : "002"
// 		,"name" : "Venti"
// 		,"description" : "God of Freedom"
// 		,"isAvailable" : true
// 		,"dateAdded" : "10-18-2021"
// }];

// console.log(assets);

// const assetsJSON = JSON.stringify(assets);

// console.log(assetsJSON);
// console.log(typeof(assetsJSON))

// const json = '{"id":"001","name":"Zhongli","description":"God of Contract","isAvailable":false,"dateAdded":"10-13-2021"}';

// const json1 = JSON.parse(json);
// console.log(json1);
// console.log(typeof(json1));


// -------------------------

let courseArr = [{
	     "id" : "001"
		,"name" : "Algebra"
		,"description" : "Math course"
		,"price" : 1000
		,"isActive" : true
},
{
	     "id" : "002"
		,"name" : "Science"
		,"description" : "Science course"
		,"price" : 2500
		,"isActive" : true
}];

const postCourse = (courseArr,id,name,description, price,isActive) => {

	let course = new Object();

	course.id = id;
	course.name = name;
	course.description = description;
	course.price = price;
	course.isActive = isActive;

	// let course = `{id:${this.id},name : ${this.name},description : ${this.description},price:${this.price}, isActive: ${this.isActive}}`;

	console.log(typeof(course));

	courseArr.push(course);
	
	console.log(courseArr[2]);

	console.log(`You have created ${course.name }. The price is ${course.price}`);

}

postCourse(courseArr,'003','Philippine History','Philippine History course', 3000, true);

const finCourse = (num) => {

	 this.num = num;

	let found = courseArr.find((a) => a.id === this.num )
	console.log(found);
}

finCourse("001");

const delCourse = () => {
	courseArr.pop();
	console.log(courseArr);
}

delCourse();